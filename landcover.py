import argparse
import numpy as np
import matplotlib.pyplot as plt

# User input

parser = argparse.ArgumentParser(description='Plot a map of the world')

parser.add_argument('latitude', metavar='F', type=float, nargs='?', const=None,
                    help='latitude')

parser.add_argument('longitude', metavar='F', type=float, nargs='?', const=None,
                    help='longitude')

input_user = parser.parse_args()

# Boolean to enable close-view
small_window = False

if input_user.longitude is not None and input_user.longitude is not None:
    x_coor = float(input_user.longitude)
    y_coor = float(input_user.latitude)

    x_coor_low = max(x_coor - 5, -180)
    x_coor_high = min(x_coor + 5, 180)

    y_coor_low = max(y_coor - 5, -90)
    y_coor_high = min(y_coor + 5, 90)

    small_window = True

# Map reading

map_file_name = 'data/gl-latlong-1km-landcover.bsq'

data = np.fromfile(map_file_name, dtype=np.uint8, count=-1, sep='', offset=0)

data = data.reshape((21600, -1))

# Geographic locations


def convert_to_degrees(degree, minutes, seconds):
    """
    Receives a degrees, minutes and seconds and convert to degrees
    """
    return (abs(degree) / degree) * (abs(degree) + minutes / 60 + seconds / 3600)


# Coodinates of corners
upper_left = ((-180, 0, 0), (90, 0, 0))
lower_right = ((180, 0, 0), (-90, 0, 0))

x_low = convert_to_degrees(*upper_left[0])
x_high = convert_to_degrees(*lower_right[0])

y_low = convert_to_degrees(*lower_right[1])
y_high = convert_to_degrees(*upper_left[1])

xs = np.linspace(x_low, x_high, num=data.shape[1], endpoint=False)
ys = np.linspace(y_low, y_high, num=data.shape[0], endpoint=False)

plt.imshow(data[::50, ::50], extent=(x_low, x_high, y_low, y_high))

if small_window:
    plt.xlim((x_coor_low, x_coor_high))
    plt.ylim((y_coor_low, y_coor_high))

# Earthquakes

earthquake_file_name = 'data/events_4.5.txt'
output_file_name = 'data/events_4.5.ext.txt'

with open(earthquake_file_name, 'r') as f, open(output_file_name, 'w') as o:
    for i, line in enumerate(f):
        if i < 7:
            o.write(line)
            continue
        row = line.split(';')
        x = float(row[2])  # long
        y = float(row[1])  # lat

        xfilter = np.searchsorted(xs, x)
        yfilter = np.searchsorted(ys, y)

        plt.plot(xs[xfilter], ys[yfilter], 'or')

        place = data[data.shape[0] - yfilter, xfilter]

        if place == 0:
            new_line = line.rstrip() + "water\n"
        else:
            new_line = line.rstrip() + "land\n"

        o.write(new_line)

plt.show()
